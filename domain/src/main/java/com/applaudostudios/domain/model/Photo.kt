package com.applaudostudios.domain.model

data class Photo(
    val uid: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String,
    val albumId: Int
)