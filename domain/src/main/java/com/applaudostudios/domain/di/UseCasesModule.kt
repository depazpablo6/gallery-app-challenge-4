package com.applaudostudios.domain.di

import com.applaudostudios.domain.interaction.*
import org.koin.dsl.module

val useCasesModule = module {
    factory<AllPhotosUseCase> { AllPhotosUseCaseImpl(photoRepository = get()) }
    factory<GetPhotoUseCase> { GetPhotoUseCaseImpl(photoRepository = get()) }
    factory<RefreshPhotosUseCase> { RefreshPhotosUseCaseImpl(photoRepository = get()) }
}