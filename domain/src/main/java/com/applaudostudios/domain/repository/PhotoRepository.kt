package com.applaudostudios.domain.repository

import com.applaudostudios.domain.model.Photo
import io.reactivex.rxjava3.core.Flowable

interface PhotoRepository {
    fun getPhotos(): Flowable<List<Photo>>
    fun refreshPhotos(): Flowable<List<Photo>>
    fun getPhoto(id: Int): Flowable<Photo>
}