package com.applaudostudios.domain.interaction

import com.applaudostudios.domain.model.Photo
import com.applaudostudios.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable

class GetPhotoUseCaseImpl(private val photoRepository: PhotoRepository) : GetPhotoUseCase {
    override fun invoke(id: Int): Flowable<Photo> = photoRepository.getPhoto(id)
}