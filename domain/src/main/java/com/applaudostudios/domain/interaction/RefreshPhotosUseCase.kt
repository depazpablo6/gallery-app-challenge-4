package com.applaudostudios.domain.interaction

import com.applaudostudios.domain.model.Photo
import io.reactivex.rxjava3.core.Flowable


interface RefreshPhotosUseCase {
    operator fun invoke(): Flowable<List<Photo>>
}