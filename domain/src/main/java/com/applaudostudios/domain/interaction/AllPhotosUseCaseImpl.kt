package com.applaudostudios.domain.interaction

import com.applaudostudios.domain.model.Photo
import com.applaudostudios.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable

class AllPhotosUseCaseImpl(private val photoRepository: PhotoRepository) : AllPhotosUseCase {
    override fun invoke(): Flowable<List<Photo>> = photoRepository.getPhotos()
}