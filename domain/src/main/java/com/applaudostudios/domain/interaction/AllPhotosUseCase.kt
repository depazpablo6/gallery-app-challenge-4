package com.applaudostudios.domain.interaction

import com.applaudostudios.domain.model.Photo
import io.reactivex.rxjava3.core.Flowable

interface AllPhotosUseCase {
    operator fun invoke(): Flowable<List<Photo>>
}