package com.applaudostudios.domain.interaction

import com.applaudostudios.domain.model.Photo
import com.applaudostudios.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable

class RefreshPhotosUseCaseImpl(private val photoRepository: PhotoRepository) :
    RefreshPhotosUseCase {
    override operator fun invoke(): Flowable<List<Photo>> = photoRepository.refreshPhotos()
}