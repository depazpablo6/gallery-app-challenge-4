package com.applaudostudios.domain.interaction

import com.applaudostudios.domain.model.Photo
import io.reactivex.rxjava3.core.Flowable

interface GetPhotoUseCase {
    operator fun invoke(id: Int): Flowable<Photo>
}