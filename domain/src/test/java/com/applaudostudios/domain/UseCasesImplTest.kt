package com.applaudostudios.domain

import com.applaudostudios.domain.interaction.AllPhotosUseCaseImpl
import com.applaudostudios.domain.interaction.GetPhotoUseCaseImpl
import com.applaudostudios.domain.interaction.RefreshPhotosUseCaseImpl
import com.applaudostudios.domain.model.Photo
import com.applaudostudios.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

@ExperimentalCoroutinesApi
internal class UseCasesImplTest {

    private val repository: PhotoRepository = mock(PhotoRepository::class.java)
    private lateinit var allPhotosUseCase: AllPhotosUseCaseImpl
    private lateinit var getPhotoUseCase: GetPhotoUseCaseImpl
    private lateinit var refreshPhotosUseCase: RefreshPhotosUseCaseImpl

    private val image1 =
        Photo(1, "image1", "http://image1.com", "http://thumbnailimage1.com", 6)
    private val image2 =
        Photo(2, "image2", "http://image2.com", "http://thumbnailimage2.com", 6)
    private val image3 =
        Photo(3, "image3", "http://image3.com", "http://thumbnailimage3.com", 6)
    private val imageList = listOf(image1, image2, image3)
    private val imageListFlowable = Flowable.just(imageList.toList())
    private val image1Flowable = Flowable.just(imageList[imageId])

    @Before
    fun mockBehavior() {
        allPhotosUseCase = AllPhotosUseCaseImpl(repository)
        getPhotoUseCase = GetPhotoUseCaseImpl(repository)
        refreshPhotosUseCase = RefreshPhotosUseCaseImpl(repository)
        `when`(repository.getPhotos()).thenReturn(imageListFlowable)
        `when`(repository.getPhoto(imageId)).thenReturn(image1Flowable)
        `when`(repository.refreshPhotos()).thenReturn(imageListFlowable)
    }

    @Test
    fun allPhotosUseCase_completed_returnsListOfPhoto() {
        repository.getPhotos().test().assertValue(imageList)
    }

    @Test
    fun getPhotoUseCase_completed_returnsAPhoto() {
        repository.getPhoto(imageId).test().assertValue(imageList[imageId])
    }

    @Test
    fun refreshPhotosUseCase_completed() {
        repository.refreshPhotos().test().assertValue(imageList)
    }

    companion object {
        private const val imageId = 0
    }
}