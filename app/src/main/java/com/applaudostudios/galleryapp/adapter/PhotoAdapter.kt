package com.applaudostudios.galleryapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudostudios.domain.model.Photo
import com.applaudostudios.galleryapp.R
import com.applaudostudios.galleryapp.util.addHeader
import com.applaudostudios.galleryapp.util.replaceHttps
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class PhotoAdapter(private val onClickItem: (Int) -> Unit) :
    ListAdapter<Photo, PhotoAdapter.PhotoViewHolder>(PhotoComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        return PhotoViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(getItem(position), onClickItem)
    }

    class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val photoTitle: TextView = itemView.findViewById(R.id.photoTitle)
        private val photoImage: ImageView = itemView.findViewById(R.id.photoImage)
        private val photoId: TextView = itemView.findViewById(R.id.photoAlbum)
        private val photoAlbum: TextView = itemView.findViewById(R.id.photoId)

        fun bind(photo: Photo, clickItem: (Int) -> Unit) {
            photoTitle.text = photo.title
            photoId.text = photo.uid.toString()
            photoAlbum.text = photo.albumId.toString()

            Glide.with(itemView)
                .load(String.addHeader(photo.thumbnailUrl.replaceHttps()))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.ic_launcher_background)
                .into(photoImage)

            itemView.setOnClickListener {
                clickItem(photo.uid)
            }
        }

        companion object {
            fun create(parent: ViewGroup): PhotoViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.photo_cards, parent, false)
                return PhotoViewHolder(view)
            }
        }
    }

    class PhotoComparator : DiffUtil.ItemCallback<Photo>() {
        override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
            return (oldItem.title == newItem.title &&
                    oldItem.uid == newItem.uid &&
                    oldItem.albumId == newItem.albumId &&
                    oldItem.thumbnailUrl == newItem.thumbnailUrl &&
                    oldItem.url == newItem.url)
        }

    }

}