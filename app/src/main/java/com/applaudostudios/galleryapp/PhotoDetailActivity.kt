package com.applaudostudios.galleryapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.applaudostudios.galleryapp.databinding.ActivityPhotoDetailBinding
import com.applaudostudios.galleryapp.util.addHeader
import com.applaudostudios.galleryapp.util.replaceHttps
import com.applaudostudios.galleryapp.viewmodel.PhotoViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.androidx.viewmodel.ext.android.viewModel

class PhotoDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPhotoDetailBinding
    private val disposable = CompositeDisposable()

    private val photoViewModel: PhotoViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPhotoDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val photoId = intent.getIntExtra(PHOTO_ID, 0)
        disposable.add(
            photoViewModel.getPhoto(photoId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.urlPhoto.text = it.url
                    binding.titlePhoto.text = it.title
                    binding.idPhoto.text = it.uid.toString()
                    binding.albumPhoto.text = it.albumId.toString()
                    setPhoto(it.url)
                }
        )
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }

    private fun setPhoto(photoUrl: String) {
        Glide.with(binding.root)
            .load(String.addHeader(photoUrl.replaceHttps()))
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .fitCenter()
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.ic_launcher_background)
            .into(binding.imageView)
    }

    companion object {
        const val PHOTO_ID = "PHOTO_ID"
    }

}