package com.applaudostudios.galleryapp.util

import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders

fun String.Companion.addHeader(url: String): GlideUrl = GlideUrl(
    url,
    LazyHeaders.Builder()
        .addHeader("User-Agent", "your-user-agent")
        .build()
)

fun String.replaceHttps(): String = replace("https", "http")