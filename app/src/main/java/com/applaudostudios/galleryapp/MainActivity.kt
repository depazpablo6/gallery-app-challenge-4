package com.applaudostudios.galleryapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.applaudostudios.domain.model.Photo
import com.applaudostudios.galleryapp.adapter.PhotoAdapter
import com.applaudostudios.galleryapp.databinding.ActivityMainBinding
import com.applaudostudios.galleryapp.viewmodel.PhotoViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var photoAdapter: PhotoAdapter

    private val photoViewModel: PhotoViewModel by viewModel()
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        createActivity()
        val postRecycler = binding.photoRecycler
        postRecycler.adapter = photoAdapter
        postRecycler.layoutManager = LinearLayoutManager(this)
    }

    override fun onStart() {
        super.onStart()
        disposable.add(
            photoViewModel.insertPhotos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn{
                    Toast.makeText(applicationContext, NETWORK_ERROR, Toast.LENGTH_SHORT).show()
                    listOf()
                }
                .subscribe {}
        )

        disposable.add(
            photoViewModel.getAllPhotos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it != photoViewModel.allPhotos || photoAdapter.currentList.isEmpty()) {
                        photoViewModel.allPhotos = it
                        photoAdapter.submitList(it)
                    }
                }
        )
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }

    private fun createActivity() {
        photoAdapter = PhotoAdapter {
            val intent = Intent(this, PhotoDetailActivity::class.java).apply {
                putExtra(PhotoDetailActivity.PHOTO_ID, it)
            }
            startActivity(intent)
        }
    }

    companion object {
        private const val NETWORK_ERROR = "Network Error: Make sure that you have Internet Access"
    }

}