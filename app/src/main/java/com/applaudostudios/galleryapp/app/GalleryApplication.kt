package com.applaudostudios.galleryapp.app

import android.app.Application
import com.applaudostudios.data.di.databaseModule
import com.applaudostudios.data.di.networkModule
import com.applaudostudios.data.di.repositoryModule
import com.applaudostudios.domain.di.useCasesModule
import com.applaudostudios.galleryapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class GalleryApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@GalleryApplication)
            modules(appModules + domainModules + dataModules)
        }
    }

}

val appModules = listOf(viewModelModule)
val domainModules = listOf(useCasesModule)
val dataModules = listOf(repositoryModule, databaseModule, networkModule)