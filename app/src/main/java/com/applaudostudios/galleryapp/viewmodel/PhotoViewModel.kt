package com.applaudostudios.galleryapp.viewmodel

import androidx.lifecycle.ViewModel
import com.applaudostudios.domain.interaction.AllPhotosUseCase
import com.applaudostudios.domain.interaction.GetPhotoUseCase
import com.applaudostudios.domain.interaction.RefreshPhotosUseCase
import com.applaudostudios.domain.model.Photo

class PhotoViewModel(
    private val refreshPhotosUseCase: RefreshPhotosUseCase,
    private val getPhotoUseCase: GetPhotoUseCase,
    private val allPhotosUseCase: AllPhotosUseCase
) : ViewModel() {

    var allPhotos: List<Photo>? = null

    fun insertPhotos() = refreshPhotosUseCase()

    fun getPhoto(id: Int) = getPhotoUseCase(id)

    fun getAllPhotos() = allPhotosUseCase()

}