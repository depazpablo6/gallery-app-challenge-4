package com.applaudostudios.galleryapp.di

import com.applaudostudios.galleryapp.viewmodel.PhotoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        PhotoViewModel(
            refreshPhotosUseCase = get(),
            getPhotoUseCase = get(),
            allPhotosUseCase = get()
        )
    }
}