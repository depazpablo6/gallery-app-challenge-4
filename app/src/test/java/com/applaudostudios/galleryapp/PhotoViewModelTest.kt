package com.applaudostudios.galleryapp

import com.applaudostudios.domain.interaction.AllPhotosUseCase
import com.applaudostudios.domain.interaction.GetPhotoUseCase
import com.applaudostudios.domain.interaction.RefreshPhotosUseCase
import com.applaudostudios.domain.model.Photo
import com.applaudostudios.galleryapp.viewmodel.PhotoViewModel
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

@ExperimentalCoroutinesApi
internal class PhotoViewModelTest {

    private val refreshPhotosUseCase: RefreshPhotosUseCase = mock(RefreshPhotosUseCase::class.java)
    private val getPhotoUseCase: GetPhotoUseCase = mock(GetPhotoUseCase::class.java)
    private val allPhotoUseCase: AllPhotosUseCase = mock(AllPhotosUseCase::class.java)
    private lateinit var viewModel: PhotoViewModel

    private val image1 =
        Photo(1, "image1", "http://image1.com", "http://thumbnailimage1.com", 6)
    private val image2 =
        Photo(2, "image2", "http://image2.com", "http://thumbnailimage2.com", 6)
    private val image3 =
        Photo(3, "image3", "http://image3.com", "http://thumbnailimage3.com", 6)
    private val imageList = listOf(image1, image2, image3)
    private val imageListFlowable = Flowable.just(imageList)
    private val image1Flowable = Flowable.just(image1)

    @Before
    fun mockBehavior() {
        `when`(getPhotoUseCase(imageId)).thenReturn(image1Flowable)
        `when`(allPhotoUseCase()).thenReturn(imageListFlowable)
        `when`(refreshPhotosUseCase()).thenReturn(imageListFlowable)
        viewModel = PhotoViewModel(refreshPhotosUseCase, getPhotoUseCase, allPhotoUseCase)
    }

    @Test
    fun getPhoto_completed_returnsOnePhoto() {
        viewModel.getPhoto(imageId).test().assertValue(image1)
    }

    @Test
    fun insertPhotos_completed_returnsListOfPhotos() {
        viewModel.insertPhotos().test().assertValue(imageList)
    }

    @Test
    fun getPhotos_completed_returnsListOfPhotos() {
        viewModel.getAllPhotos().test().assertValue(imageList)
    }

    companion object {
        private const val imageId = 0
    }

}