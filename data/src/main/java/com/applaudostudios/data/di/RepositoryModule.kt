package com.applaudostudios.data.di

import com.applaudostudios.data.repository.PhotoRepositoryImpl
import com.applaudostudios.domain.repository.PhotoRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<PhotoRepository> { PhotoRepositoryImpl(photoDao = get(), photoService = get()) }
}