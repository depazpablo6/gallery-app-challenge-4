package com.applaudostudios.data.di

import com.applaudostudios.data.database.GalleryDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single { GalleryDatabase.getDatabase(androidContext()) }
    factory { get<GalleryDatabase>().photoDao() }
}