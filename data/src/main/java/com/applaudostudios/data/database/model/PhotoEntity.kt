package com.applaudostudios.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudostudios.data.networking.base.DomainMapper
import com.applaudostudios.domain.model.Photo
import com.google.gson.annotations.SerializedName

@Entity
data class PhotoEntity(
    @field:SerializedName("id") @PrimaryKey val uid: Int,
    @field:SerializedName("title") val title: String,
    @field:SerializedName("url") val url: String,
    @field:SerializedName("thumbnailUrl") val thumbnailUrl: String,
    @field:SerializedName("albumId") val albumId: Int
) : DomainMapper<Photo> {
    override fun mapToDomainModel() = Photo(uid, title, url, thumbnailUrl, albumId)
}