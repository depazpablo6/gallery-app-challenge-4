package com.applaudostudios.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.applaudostudios.data.database.dao.PhotoDao
import com.applaudostudios.data.database.model.PhotoEntity

@Database(entities = [PhotoEntity::class], version = 1, exportSchema = false)
abstract class GalleryDatabase : RoomDatabase() {

    abstract fun photoDao(): PhotoDao

    companion object {
        fun getDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            GalleryDatabase::class.java,
            "gallery_database"
        ).build()
    }
}