package com.applaudostudios.data.database.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.database.model.PhotoEntity
import io.reactivex.rxjava3.core.Flowable


@Dao
interface PhotoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(photos: List<PhotoEntity>)

    @Query("DELETE FROM PhotoEntity")
    fun deleteAll()

    @Query("SELECT * FROM PhotoEntity")
    fun getAllPhotos(): Flowable<List<PhotoEntity>>

    @Query("SELECT * FROM PhotoEntity WHERE uid = :uid")
    fun getPhoto(uid: Int): Flowable<PhotoEntity>
}