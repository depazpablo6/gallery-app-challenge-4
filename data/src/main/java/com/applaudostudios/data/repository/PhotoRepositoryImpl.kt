package com.applaudostudios.data.repository

import com.applaudostudios.data.database.dao.PhotoDao
import com.applaudostudios.data.networking.PhotoApi
import com.applaudostudios.data.utils.photosNumber
import com.applaudostudios.domain.model.Photo
import com.applaudostudios.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.net.UnknownHostException


class PhotoRepositoryImpl(private val photoDao: PhotoDao, private val photoService: PhotoApi) :
    PhotoRepository {


    override fun getPhotos(): Flowable<List<Photo>> =
        photoDao.getAllPhotos().map { it ->
            it.map { PhotoEntity ->
                PhotoEntity.mapToDomainModel()
            }
        }

    override fun refreshPhotos(): Flowable<List<Photo>> {
        return try {
            photoService.getAllPhotos(6).doOnNext {
                photoDao.insertAll(it.take(photosNumber))
            }.map {
                it.map { photoEntity ->
                    photoEntity.mapToDomainModel()
                }
            }.subscribeOn(Schedulers.io())
        } catch (e: UnknownHostException) {
            Flowable.just(listOf<Photo>())
        }
    }

    override fun getPhoto(id: Int): Flowable<Photo> =
        photoDao.getPhoto(id).map { it ->
            it.mapToDomainModel()
        }

}