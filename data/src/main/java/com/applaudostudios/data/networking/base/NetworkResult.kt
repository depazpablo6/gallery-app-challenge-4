package com.applaudostudios.data.networking.base

interface DomainMapper<T : Any> {
    fun mapToDomainModel(): T
}