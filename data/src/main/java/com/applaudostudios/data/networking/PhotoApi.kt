package com.applaudostudios.data.networking

import com.applaudostudios.data.database.model.PhotoEntity
import io.reactivex.rxjava3.core.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoApi {

    @GET("photos")
    fun getAllPhotos(@Query("albumId") albumId: Int): Flowable<List<PhotoEntity>>

}