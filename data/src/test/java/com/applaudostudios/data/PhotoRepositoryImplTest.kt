package com.applaudostudios.data

import com.applaudostudios.data.database.dao.PhotoDao
import com.applaudostudios.data.database.model.PhotoEntity
import com.applaudostudios.data.networking.PhotoApi
import com.applaudostudios.data.repository.PhotoRepositoryImpl
import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.core.IsEqual
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
internal class PhotoRepositoryImplTest {

    private val photoDao: PhotoDao = mock(PhotoDao::class.java)
    private val photoService: PhotoApi = mock(PhotoApi::class.java)
    private val photoServiceException: PhotoApi = mock(PhotoApi::class.java)
    private lateinit var repository: PhotoRepositoryImpl
    private lateinit var repositoryException: PhotoRepositoryImpl

    private val image1 =
        PhotoEntity(1, "image1", "http://image1.com", "http://thumbnailimage1.com", 6)
    private val image1Domain = image1.mapToDomainModel()
    private val image2 =
        PhotoEntity(2, "image2", "http://image2.com", "http://thumbnailimage2.com", 6)
    private val image3 =
        PhotoEntity(3, "image3", "http://image3.com", "http://thumbnailimage3.com", 6)
    private val imageList = listOf(image1, image2, image3)
    private val imageListDatabase = mutableListOf<PhotoEntity>()
    private val imageListDomain = imageList.map { it.mapToDomainModel() }
    private val imageListFlowable = Flowable.just(imageList.toList())
    private val image1Flowable = Flowable.just(imageList[imageId])

    @Before
    fun createRepository() {

        `when`(photoService.getAllPhotos(albumId)).thenReturn(imageListFlowable)
        `when`(photoDao.getAllPhotos()).thenReturn(imageListFlowable)
        `when`(photoDao.getPhoto(imageId)).thenReturn(image1Flowable)
        `when`(photoDao.deleteAll()).then { imageListDatabase.clear() }
        `when`(photoDao.insertAll(imageList)).then { imageListDatabase.addAll(imageList) }
        `when`(photoServiceException.getAllPhotos(albumId)).then { throw UnknownHostException() }
        repository = PhotoRepositoryImpl(photoDao, photoService)
        repositoryException = PhotoRepositoryImpl(photoDao, photoServiceException)
    }

    companion object {
        private const val imageId = 0
        private const val albumId = 6
    }

    @Test
    fun getPhotos_completed_returnsListOfPhoto() {
        repository.getPhotos().test().assertValue(imageListDomain)
    }

    @Test
    fun getPhoto_completed_returnsOnePhoto() {
        repository.getPhoto(imageId).test().assertValue(image1Domain)
    }

    @Test
    fun refreshPhotos_completed() {
        repository.refreshPhotos().test()
        assertThat(imageListDatabase, IsEqual(imageList))
    }

    @Test
    fun refreshPhotos_notCompleted_throwsUnknownHostException() {
        repositoryException.refreshPhotos().test()
        assertTrue(imageListDatabase.isEmpty())
    }

}